const searchEngines = {
    yandex: {
        url: 'https://yandex.by/',
        inputField: '.input__control',
        button: 'button[type=submit]',
        result: 'ul.serp-list.serp-list_left_yes li a'
    },
    google: {
        url: 'https://www.google.com/',
        inputField: '#searchform form input[maxlength=\'2048\']',
        button: '#searchform form .FPdoLc.VlcLAe input[type=\'submit\']',
        result: '#search #ires a'
    }
};

module.exports = searchEngines;
