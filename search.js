const puppeteer = require('puppeteer');
const settings = require('./searchEngine');

async function getValue(element, property)  {
    const valueHandle = await element.getProperty(property);
    return await valueHandle.jsonValue();
}

const getSearchResult = async (page, searchEngine) =>  {
    const result = {};
    const element = await page.$(searchEngine.result);
    if (element) {
        result.title =  await getValue(element, 'innerText');
        result.link = await getValue(element, 'href');
        return result;
    }
    return null;
};

async function search(searchEngine, searchQuery) {

    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto(settings[searchEngine].url);
    await page.type(settings[searchEngine].inputField, searchQuery);
    await page.click(settings[searchEngine].button);
    await page.waitForNavigation();

    const result = await getSearchResult(page, settings[searchEngine]);
    console.log(result);
    await page.screenshot({path: `./screen/${searchEngine}-${new Date().getTime()}.png`});

    await browser.close();
}

const printUserQuestion = text => {
    const standard_input = process.stdin;
    standard_input.setEncoding('utf-8');
    console.log(text);

    return new Promise(resolve => {
        standard_input.on('data', data =>   resolve(data.trim()) );
    });
};

const isSearchEngineExist = (searchEngine) => {
    return searchEngine in settings;
};

async function startApp () {
    let searchEngine, searchQuery;
    await printUserQuestion('Укажите поисковую систему (yandex или google)').then(data => searchEngine = data);
    await printUserQuestion('Укажите запрос').then(data => searchQuery = data);

    if (isSearchEngineExist(searchEngine)) {
        search(searchEngine, searchQuery);
    } else {
        console.log('Укажите верный поисковик');
    }

}

startApp();