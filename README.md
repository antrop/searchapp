# Getting Started

## Installation

To use this project, run:

```
npm i
```

### Usage

To run app - execute on the command line:

```
npm run start
```

